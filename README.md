# Week_10 Portfolio

![](static/week-10/fullscreen.png)

## Description

This is the weekly assignment for the ten week. Its a profesional portfolio. This are the main points of the page:

- It is using gatsby and tailwind
- Its using JSON for the projectas and the tags
- It is generating programmatically the project pages from the JSON
- The images have lazy loading
- The css is processing with postCSS and purgeCSS

If you want to tested in real time use this url: https://week-10-porfolio.vercel.app/
