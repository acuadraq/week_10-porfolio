import React, { useState } from "react"
import { IProject } from "../../interfaces/projects"
import { LazyLoadImage } from "react-lazy-load-image-component"
import Layout from "../components/Layout"

type projectProps = {
  pageContent: IProject
}

function project2ndTemplate(props) {
  const { pageContext } = props
  const { pageContent: project }: projectProps = pageContext
  const [currentImage, setCurrentImage] = useState<string>(
    project.fullscreen.images[0]
  )
  const [imgId, setImgId] = useState<number>(0)

  const changeImg = (id: number) => {
    setImgId(id)
    setCurrentImage(project.fullscreen.images[id])
  }

  return (
    <Layout>
      <div className="text-gray-600 body-font">
        <div className="container px-5 py-10 mx-auto flex flex-col">
          <div className="lg:w-4/6 mx-auto">
            <div>
              <div className="rounded-lg overflow-hidden shadow-lg">
                <LazyLoadImage
                  alt="content"
                  className="object-cover object-center h-full w-full"
                  src={currentImage}
                />
              </div>
              <div className="flex flex-wrap md:gap-3 mt-5 w-full">
                {project.fullscreen.images.map((image, index) => (
                  <div key={index}>
                    <LazyLoadImage
                      onClick={() => changeImg(index)}
                      src={image}
                      className={`object-cover mb-3 md:mb-0 w-12 md:w-32 object-center h-10 md:h-20 border-hoverblue mr-5 shadow-sm ${
                        imgId === index
                          ? "border-4"
                          : "border-2 cursor-pointer hover:shadow-lg"
                      }`}
                    />
                  </div>
                ))}
              </div>
            </div>
            <div className="flex flex-col sm:flex-row mt-10">
              <div className="sm:w-1/3 text-center sm:pr-8 pb-8 md:pb-0">
                <div className="flex flex-col items-center text-center justify-center">
                  <h2 className="font-bold mt-4 text-darkcolor text-xl">
                    {project.title}
                  </h2>
                  <div className="w-12 h-1 bg-darkcolor rounded mt-2 mb-4"></div>
                  <p className="mt-2 text-base">
                    {project.fullscreen.description}
                  </p>
                </div>
              </div>
              <div className="sm:w-2/3 sm:pl-8 md:ml-12 sm:border-l border-gray-200 sm:border-t-0 border-t sm:mt-0 stext-left">
                <h2 className="font-bold text-darkcolor mt-4 text-lg">
                  Functionality:
                </h2>
                <ul className="list-disc md:list-inside mt-3">
                  {project.fullscreen.points.map((point, index) => (
                    <li key={index} className="leading-7 ml-2">
                      {point}
                    </li>
                  ))}
                </ul>
                <div className="flex justify-start mt-5">
                  <a
                    href={project.repository}
                    target="_blank"
                    className="flex items-center font-bold tracking-widest hover:bg-hoverdark cursor-pointer uppercase text-textnav bg-darkcolor border-0 py-2 px-3  rounded text-sm"
                  >
                    <img
                      src="/icons/github_2.png"
                      alt="Github"
                      className="h-4 w-4 mr-2"
                    />
                    Repository
                  </a>
                  <a
                    href={project.website}
                    target="_blank"
                    className="ml-4 flex items-center font-bold tracking-widest hover:bg-hoverlastblue cursor-pointer text-textnav uppercase bg-thisblue border-0 py-2 px-3 rounded text-sm"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-4 w-4 mr-2"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
                      />
                    </svg>
                    Website
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default project2ndTemplate
