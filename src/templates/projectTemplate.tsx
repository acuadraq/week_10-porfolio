import React, { useState } from "react"
import { IProject } from "../../interfaces/projects"
import { LazyLoadImage } from "react-lazy-load-image-component"
import Layout from "../components/Layout"

type projectProps = {
  pageContent: IProject
}

function projectTemplate(props) {
  const { pageContext } = props
  const { pageContent: project }: projectProps = pageContext
  const [currentImage, setCurrentImage] = useState<string>(
    project.fullscreen.images[0]
  )
  const [imgId, setImgId] = useState<number>(0)

  const changeImg = (id: number) => {
    setImgId(id)
    setCurrentImage(project.fullscreen.images[id])
  }

  return (
    <Layout>
      <div className="py-16">
        <div className="xl:container px-4 sm:px-8 lg:px-16 xl:px-24 mx-auto">
          <div className="hero-wrapper grid grid-cols-1 lg:grid-cols-12 gap-8 items-center lg:items-start">
            <div className="hero-text col-span-6 mt-3  md:pr-3">
              <h1 className="font-bold text-4xl max-w-xl text-darkcolor leading-tight">
                {project.title}
              </h1>
              <hr className="w-40 h-1 bg-darkcolor rounded-full mt-3" />
              <p className="mt-2 text-md text-justify leading-9">
                {project.fullscreen.description}
              </p>
              <h4 className="mt-2 font-bold text-darkcolor text-lg">
                Functionality:
              </h4>
              <ul className=" list-disc md:list-inside mt-2">
                {project.fullscreen.points.map((point, index) => (
                  <li key={index} className="text-sm leading-7 ml-2">
                    {point}
                  </li>
                ))}
              </ul>
              <div className="flex justify-start mt-5">
                <a
                  href={project.repository}
                  target="_blank"
                  className="flex items-center font-bold tracking-widest hover:bg-hoverdark cursor-pointer uppercase text-textnav bg-darkcolor border-0 py-2 px-3  rounded text-sm"
                >
                  <img
                    src="/icons/github_2.png"
                    alt="Github"
                    className="h-4 w-4 mr-2"
                  />
                  Repository
                </a>
                <a
                  href={project.website}
                  target="_blank"
                  className="ml-4 flex items-center font-bold tracking-widest hover:bg-hoverlastblue cursor-pointer text-textnav uppercase bg-thisblue border-0 py-2 px-3 rounded text-sm"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
                    />
                  </svg>
                  Website
                </a>
              </div>
            </div>
            <div className="hero-image col-span-6 border-3 grid justify-items-center border-textnav">
              <LazyLoadImage
                src={currentImage}
                className="object-cover shadow-lg"
                height="500"
              />

              <div className="flex flex-wrap md:gap-3 mt-5 w-full">
                {project.fullscreen.images.map((image, index) => (
                  <div key={index}>
                    <LazyLoadImage
                      onClick={() => changeImg(index)}
                      src={image}
                      className={`object-cover mb-3 md:mb-0 w-32 object-center h-20 border-hoverblue mr-5 shadow-sm ${
                        imgId === index
                          ? "border-4"
                          : "border-2 cursor-pointer hover:shadow-lg"
                      }`}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default projectTemplate
