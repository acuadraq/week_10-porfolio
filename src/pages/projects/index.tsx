import { Link } from "gatsby"
import React, { useState } from "react"
import Projects from "../../../content/Projects.json"
import Tags from "../../../content/Tags.json"
import { IProject } from "../../../interfaces/projects"
import { LazyLoadImage } from "react-lazy-load-image-component"
import Layout from "../../components/Layout"

export default function ProjectsPage() {
  const [elements, setElements] = useState<IProject[]>(Projects.content)

  const filterProjects = (value: string) => {
    if (value === "All") {
      setElements(Projects.content)
      return
    }
    setElements(
      Projects.content.filter(element => element.tags.includes(parseInt(value)))
    )
  }

  const nameTag = (id: number): string => {
    const retTag = Tags.content.filter(thistag => thistag.id === id)
    return retTag[0].name
  }

  return (
    <Layout>
      <div className="py-10 2xl:container px-4 sm:px-8 lg:px-16 xl:px-24 gap-8 mx-auto">
        <div className="flex flex-wrap justify-between">
          <h1 className="text-2xl uppercase text-darkcolor font-bold tracking-widest">
            Projects
          </h1>
          <select
            name="filter"
            className="w-full mt-2 md:w-40 p-2 border-0 rounded-lg focus:outline-none focus:ring-2 focus:ring-babyblue focus:border-transparent"
            id="filter"
            onChange={e => filterProjects(e.target.value)}
          >
            <option value="All">All</option>
            {Tags.content.map(tag => (
              <option key={tag.id} value={tag.id}>
                {tag.name}
              </option>
            ))}
          </select>
        </div>
        <hr className="h-1 bg-thisblue rounded-full mt-5" />
        <div className="grid sm:grid-cols-2 xl:grid-cols-3 gap-8 mt-8">
          {elements.map(project => (
            <div
              key={project.id}
              className="overflow-hidden bg-white rounded-lg shadow-md"
            >
              <div className="h-full bg-darkcolor">
                <Link to={`/projects${project.slug}`}>
                  <LazyLoadImage
                    className="w-full h-56 object-cover object-center"
                    src={project.grid.image}
                    alt={project.title}
                  />
                </Link>
                <div className="p-6">
                  <div className="flex justify-between flex-wrap items-center mb-3">
                    {project.tags.map(tag => (
                      <span
                        key={tag}
                        className="rounded-lg text-xs font-bold uppercase p-1 bg-babyblue"
                      >
                        {nameTag(tag)}
                      </span>
                    ))}
                  </div>
                  <h1 className="font-bold text-lg text-textnav mb-3 ">
                    <Link
                      to={`/projects${project.slug}`}
                      className="hover:text-babyblue"
                    >
                      {project.title}
                    </Link>
                  </h1>
                  <p className="leading-relaxed mb-3 text-textnav">
                    {project.grid.description}
                  </p>
                  <div className="flex justify-between flex-wrap mt-3">
                    <a
                      href={project.repository}
                      target="_blank"
                      className="flex items-center text-darkcolor uppercase font-bold text-sm bg-textnav rounded-md hover:bg-hoverbutton px-2 py-1"
                    >
                      <img
                        src="/icons/github.png"
                        alt="Github"
                        className="h-4 w-4 mr-1"
                      />
                      Repository
                    </a>
                    <a
                      href={project.website}
                      target="_blank"
                      className="flex items-center text-darkcolor uppercase font-bold text-sm bg-textnav rounded-md hover:bg-hoverbutton px-2 py-1"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-4 w-4 mr-1"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
                        />
                      </svg>
                      Website
                    </a>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </Layout>
  )
}
