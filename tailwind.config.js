module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      darkcolor: "#1B262C",
      textnav: "#E8F0F2",
      babyblue: "#BBE1FA",
      lightblue: "#3282B8",
      hoverblue: "#B3D5EB",
      thisblue: "#0F4C75",
      hoverbutton: "#DCE7EA",
      hoverdark: "#2B3B44",
      hoverlastblue: "#1D5E89",
    },
    fontFamily: {
      poppins: ["Poppins", "sans-serif"],
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
