export interface IProjects {
  title: string
  content: {
    id: number
    title: string
    slug: string
    grid: {
      description: string
      image: string
    }
    fullscreen: {
      description: string
      points: string[]
      images: string[]
    }
    tags: number[]
    repository: string
    website: string
  }[]
}

export interface IProject {
  id: number
  title: string
  slug: string
  grid: {
    description: string
    image: string
  }
  fullscreen: {
    description: string
    points: string[]
    images: string[]
  }
  tags: number[]
  repository: string
  website: string
}
