export interface ITags {
  title: string
  content: {
    id: number
    name: string
    icon: string
  }[]
}
