const fs = require("fs")

exports.createPages = ({ actions }) => {
  const { createPage } = actions
  const data = JSON.parse(
    fs.readFileSync("./content/Projects.json", { encoding: "utf-8" })
  )
  data.content.forEach(element => {
    createPage({
      path: `projects${element.slug}`,
      component: require.resolve(
        `${
          element.layout === "first"
            ? "./src/templates/projectTemplate.tsx"
            : "./src/templates/project2ndTemplate.tsx"
        }`
      ),
      context: {
        pageContent: element,
      },
    })
  })
}
